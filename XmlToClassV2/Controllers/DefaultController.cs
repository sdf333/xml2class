﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace XmlToClassV2.Controllers
{
    public class HomeController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            var model = new MyModel();
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Index(MyModel model)
        {
            var guid = Guid.NewGuid().ToString("N");
            var xsdPath = Path.Combine(HttpRuntime.BinDirectory, "xsd.exe");
            var fullPathXml = Path.Combine(HttpRuntime.BinDirectory, guid + ".xml");
            var fullPathXsd = Path.Combine(HttpRuntime.BinDirectory, guid + ".xsd");
            System.IO.File.WriteAllText(fullPathXml, model.xml);


            var cmd1 = "\"" + xsdPath + "\"" + " \"" + fullPathXml + "\"" + " /o:\"" +
                      HttpRuntime.BinDirectory.TrimEnd('\\') + "\"";

            var cmd2 = "\"" + xsdPath + "\"" + " \"" + fullPathXsd + "\"" + " /c /o:\"" +
                      HttpRuntime.BinDirectory.TrimEnd('\\') + "\"";

            Process MyProcess = new Process();
            MyProcess.StartInfo.FileName = "cmd.exe";
            MyProcess.StartInfo.UseShellExecute = false;
            MyProcess.StartInfo.RedirectStandardInput = true;
            MyProcess.StartInfo.RedirectStandardOutput = true;
            MyProcess.StartInfo.RedirectStandardError = true;
            MyProcess.StartInfo.CreateNoWindow = true;
            if (MyProcess.Start())
            {
                MyProcess.StandardInput.WriteLine(cmd1);
                MyProcess.StandardInput.WriteLine(cmd2);
                MyProcess.Close();
            }
            Thread.Sleep(1000);
            model.classText = System.IO.File.ReadAllText(Path.Combine(HttpRuntime.BinDirectory, guid + ".cs"));
            return View(model);
        }
    }
}