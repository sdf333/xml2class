﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace XmlToClassV2.Controllers
{
    public class MyModel 
    {
         
        public string xml { get; set; }
        public string classText { get; set; }
    }
}